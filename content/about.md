---
title: "About Laiot..."
layout: "about"
url: "/about/"
summary: about
---
I'm Carmelo, a 24yo Italian guy from Sicily.
I'm currently working as a Technical Graduate at Red Hat.

Right now I'm focusing on:
+ Cloud-Native Development Solutions
+ Advantages of the Rust Programming Language
+ Fedora Packaging
